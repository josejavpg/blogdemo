//
//  ViewController.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 4/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import UIKit
import RealmSwift
import BlogDemoCore

final class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var sourcePosts: Results<Post>?
    
    var selectedPostIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.backgroundColor = #colorLiteral(red: 0, green: 0.7030938864, blue: 0, alpha: 1)
        navigationItem.standardAppearance = appearance
        self.title = "Posts"
        setupSegmentedControlUI()
        APIManager.getDataFromServer {
            self.getData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getData()
    }
    
    fileprivate func setupSegmentedControlUI() {
        self.segmentedControl.layer.borderColor = #colorLiteral(red: 0, green: 0.7030938864, blue: 0, alpha: 1)
        self.segmentedControl.layer.borderWidth = 1.0
        let selectedTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let normalTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 0, green: 0.7030938864, blue: 0, alpha: 1)]
        segmentedControl.setTitleTextAttributes(normalTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(selectedTextAttributes, for: .selected)
    }
    
    /// Action handler to show all the post or only the favoires
    @IBAction private func selectPost(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 1:
                self.sourcePosts = PostsStore.getFavoritesPosts()
            default:
                self.sourcePosts = PostsStore.getAllPosts()
        }
        self.tableView.reloadData()
    }
    
    /// Handler to bring and populate the data from the server.
    @IBAction private func fetchDataFromServer() {
        PostsStore.fetchPosts {
            DispatchQueue.main.async {
                self.segmentedControl.selectedSegmentIndex = 0
                self.sourcePosts = PostsStore.getAllPosts()
                self.tableView.reloadData()
            }
        }
    }
    
    /// Method used to instantiate PostDetailViewController prior the visualization.
    @IBSegueAction private func showPostDetail(_ coder: NSCoder) -> PostDetailViewController? {
        guard let selectedPost = self.sourcePosts?[self.selectedPostIndex] else { return nil}
        return PostDetailViewController(coder: coder, post: selectedPost)
    }
    
    /// Call PostStore to delete all the posts from the local db
    @IBAction private func deleteAllPosts(_ sender: Any) {
        
        var arrayIndexPath: [IndexPath] = []
        guard let posts = self.sourcePosts else { return }
        if posts.count > 0 {
            for index in 1...posts.count {
                arrayIndexPath.append(IndexPath(row: index - 1, section: 0))
            }
            
            PostsStore.deletAll(posts) {
                DispatchQueue.main.async {
                    self.tableView.deleteRows(at: arrayIndexPath, with: .automatic)
                    self.sourcePosts = nil
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    /// Get all the posts from the local db
    /// in case the local db is empty it will request the data from the server
    private func getData() {
        self.sourcePosts = PostsStore.getAllPosts()
        self.segmentedControl.selectedSegmentIndex = 0
        if sourcePosts?.count ?? 0 > 0 {
            self.tableView.reloadData()
        } else {
            self.fetchDataFromServer()
        }
    }
    
}

