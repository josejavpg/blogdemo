//
//  PostTableViewCell.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 4/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var tile: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(index: Int, _ isFavorite: Bool, _ wasReaded: Bool ) -> UIImage? {
        if isFavorite {
            return UIImage(systemName: "star.fill")
        } else if index < 20 && wasReaded == false {
            return UIImage(systemName: "circle.fill")
        } else {
            return UIImage()
        }
    }

}
