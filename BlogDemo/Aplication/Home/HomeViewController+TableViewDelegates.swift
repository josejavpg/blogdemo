//
//  HomeViewController+TableviewExtensions.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 5/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sourcePosts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell",
                                                     for: indexPath) as? PostTableViewCell,
            let post = self.sourcePosts?[indexPath.row]
            else {
                return UITableViewCell()
        }
    
        cell.tile.text = post.title.capitalized
        cell.icon.image = cell.setImage(index: indexPath.row,
                                        post.isFavorite,
                                        post.wasReaded)
        cell.icon.tintColor = post.isFavorite ? #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1) : #colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedPostIndex = indexPath.row
        performSegue(withIdentifier: "ShowPostDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete, let post = self.sourcePosts?[indexPath.row] else { return }
        PostsStore.delete(post) {
            DispatchQueue.main.async {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
}
