//
//  PostDetail+TableViewDelegates.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 5/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import UIKit

extension PostDetailViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "COMMENTS"
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as? CommentTableViewCell else {
            return UITableViewCell()
        }
        
        cell.body.text = self.comments?[indexPath.row].body
        
        return cell
    }
    
}
