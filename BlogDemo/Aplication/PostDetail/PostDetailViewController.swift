//
//  PostDetailViewController.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 4/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import UIKit
import RealmSwift
import BlogDemoCore

class PostDetailViewController: UIViewController {

    
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var userWebsite: UILabel!
    @IBOutlet weak var favoriteItem: UIBarButtonItem!
    
    var user: User?
    var comments: Results<Comment>?
    var post: Post?
    
    init?(coder: NSCoder, post: Post) {
      self.post = post
      super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.backgroundColor = #colorLiteral(red: 0, green: 0.7030938864, blue: 0, alpha: 1)
        navigationItem.standardAppearance = appearance
        self.title = "Post"
        loadData()
    }
    
    private func loadData() {
        guard let post = self.post else {
            return
        }
        self.favoriteItem.image = post.isFavorite ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
        PostsStore.read(post)
        self.body.text = post.body
        showUserInformation()
        loadComments()
    }
    
    private func showUserInformation() {
        UserStore.getUser(by: post?.userId ?? 1) { [weak self] user in
            DispatchQueue.main.async {
                self?.userName.text = user.name
                self?.userEmail.text = user.email
                self?.userPhone.text = user.phone
                self?.userWebsite.text = user.website
            }
        }
    }
    
    private func loadComments() {
        CommentStore.getCommets(by: post?.id ?? 1) { comments in
            self.comments = comments
            self.tableView.reloadData()
        }
    }
    
    @IBAction private func setAsFavorite(_ sender: Any) {
        guard let post = self.post else {
            return
        }
        
        PostsStore.changeFavoriteStatus(post) {
            DispatchQueue.main.async {
                self.favoriteItem.image = post.isFavorite ? UIImage(systemName: "star.fill") : UIImage(systemName: "star")
            }
        }
    }
}
