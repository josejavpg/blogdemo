[![Swift Version][swift-image]][swift-url]
[![License][license-image]][license-url]

# BlogDemo

BlogDemo is an iOS app that lists all messages and their details from [JSONPlaceholder](https://choosealicense.com/licenses/mit/).
The data provided by the API, is persisted on the device using [Realm](https://github.com/realm/realm-cocoa).
The tool selected to handle libreries was [Swift Package Manager](https://swift.org/package-manager/).

## Requirements

- iOS 13.0+
- Xcode 11.0+

## Installation

To execute this project, you will need to clone/download look the file called `BlogDemo.xproject` the project and open it with Xcode version 11.0 or above.

## Libreries

- [Realm](https://github.com/realm/realm-cocoa)

This library was selected because is a lot easier than the native CoreData, is fast, has RealmStudio to watch all your stored data in a clear way.

## Architecture

This is an *MVC* app, that has *StoryBoards* in order to help with the interface builder and a framework calle *BlogDemoCore* where live the business loci of the project.
