//
//  Comment.swift
//  BlogDemoCore
//
//  Created by Jose Javier Pabon on 5/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import RealmSwift

public class Comment: Object, Codable {
    @objc dynamic var id = 0
    @objc dynamic var postId = 0
    @objc dynamic var body = ""
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
}
