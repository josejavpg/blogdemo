//
//  CommentStore.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 5/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import RealmSwift

public struct CommentStore {
    public static func fetchComments(using session: URLSession = .shared, _ completion: @escaping () -> Void) {
        session.request(.comments) { data, response, error in
            
            guard let data = data, error == nil else {
                fatalError("Networking error \(String(describing: error)), \(String(describing: response))")
            }
            
            do{
                let decoder = JSONDecoder()
                let commentsResponse = try decoder.decode([Comment].self, from: data)
                do {
                    let realm = try Realm()
                    
                    for comment in commentsResponse {
                        try realm.write {
                            realm.add(comment, update: .modified)
                        }
                    }
                } catch {
                    fatalError("Error opening realm: \(error)")
                }
                completion()
                
            } catch {
                fatalError("Decoding error \(error)")
            }
        }
    }
    
    public static func getCommets(by postId: Int, _ completion: @escaping (_ comments: Results<Comment>) -> Void) {
        do {
            let realm = try Realm()
            let comments = realm.objects(Comment.self).filter("postId = \(postId)")
            completion(comments)
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
}
