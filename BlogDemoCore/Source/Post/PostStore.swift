//
//  PostStore.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 4/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import RealmSwift

public struct PostsStore {
    public static func fetchPosts(using session: URLSession = .shared, _ completion: @escaping () -> Void) { //_ posts: [Post]
        session.request(.posts) { data, response, error in
            
            guard let data = data, error == nil else {
                fatalError("Networking error \(String(describing: error)), \(String(describing: response))")
            }
            
            do{
                let decoder = JSONDecoder()
                let postsResponse = try decoder.decode([Post].self, from: data)
                do {
                    let realm = try Realm()
                    
                    for post in postsResponse {
                        try realm.write {
                            realm.add(post, update: .modified)
                        }
                    }
                } catch {
                    fatalError("Error opening realm: \(error)")
                }
                completion()
                
            } catch {
                fatalError("Decoding error \(error)")
            }
        }
    }
    
    public static func getAllPosts() -> Results<Post> {
        do {
            let realm = try Realm()
            let posts = realm.objects(Post.self).sorted(byKeyPath: "id")
            return posts//completion(posts)
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
    
    public static func getFavoritesPosts() -> Results<Post> {
        do {
            let realm = try Realm()
            let posts = realm.objects(Post.self).filter("isFavorite = true").sorted(byKeyPath: "id")
            return posts
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
    
    public static func deletAll(_ posts: Results<Post>, _ completion: @escaping () -> Void)  {
        do {
            let realm = try Realm()
            try realm.write {
                realm.delete(posts)
            }
            completion()
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
    
    public static func read(_ post: Post) {
        do {
            let realm = try Realm()
            try realm.write {
                post.wasReaded = true
            }
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
    
    public static func changeFavoriteStatus(_ post: Post, _ completion: @escaping () -> Void) {
        do {
            let realm = try Realm()
            try realm.write {
                post.isFavorite = !post.isFavorite
            }
            completion()
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
    
    public static func delete(_ post: Post, _ completion: @escaping () -> Void) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.delete(post)
            }
            completion()
        } catch  {
            fatalError("Error opening realm: \(error)")
        }
    }
    
}
