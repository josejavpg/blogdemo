//
//  Post.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 4/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import RealmSwift

public class Post: Object, Decodable {
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var userId = 0
    @objc dynamic var isFavorite = false
    @objc dynamic var wasReaded = false
    
    public override class func primaryKey() -> String? {
        return "id"
    }
    
    /// Needed to  decode specific properties,
    /// because `isFavorite` and  `wasReaded` are not in the response
    private enum CodingKeys: String, CodingKey {
        case id, title, body, userId
    }
}
