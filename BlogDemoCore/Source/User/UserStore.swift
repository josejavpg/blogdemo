//
//  UserStore.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 5/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation
import RealmSwift

public struct UserStore {
    public static func fetchUsers(using session: URLSession = .shared, _ completion: @escaping () -> Void) {
        session.request(.users) { data, response, error in
            
            guard let data = data, error == nil else {
                fatalError("Networking error \(String(describing: error)), \(String(describing: response))")
            }
            
            do{
                let decoder = JSONDecoder()
                let usersResponse = try decoder.decode([User].self, from: data)
                do {
                    let realm = try Realm()
                    
                    for user in usersResponse {
                        try realm.write {
                            realm.add(user, update: .modified)
                        }
                    }
                } catch {
                    fatalError("Error opening realm: \(error)")
                }
                completion()
                
            } catch {
                fatalError("Decoding error \(error)")
            }
        }
    }
    
    public static func getUser(by id: Int, _ completion: @escaping (_ user: User) -> Void) {
        do {
            let realm = try Realm()
            guard let user = realm.object(ofType: User.self, forPrimaryKey: id)
                else { return }
            completion(user)
            //return user
        } catch {
            fatalError("Error opening realm: \(error)")
        }
    }
    
}
