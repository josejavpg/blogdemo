//
//  BlogDemoCore.h
//  BlogDemoCore
//
//  Created by Jose Javier Pabon on 5/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BlogDemoCore.
FOUNDATION_EXPORT double BlogDemoCoreVersionNumber;

//! Project version string for BlogDemoCore.
FOUNDATION_EXPORT const unsigned char BlogDemoCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BlogDemoCore/PublicHeader.h>


