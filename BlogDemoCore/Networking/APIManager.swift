//
//  APIManager.swift
//  BlogDemo
//
//  Created by Jose Javier Pabon on 4/04/20.
//  Copyright © 2020 Jose Javier Pabon. All rights reserved.
//

import Foundation

struct APIManager {
    static func getDataFromServer(_ completion: @escaping () -> Void) {
        UserStore.fetchUsers {
            print("Users Fetched")
        }
        
        CommentStore.fetchComments {
            print("Comments Fetched")
        }
        
        completion()
    }
}


private extension URL {
    static func makeForEndpoint(_ endpoint: String) -> URL {
        URL(string: "https://jsonplaceholder.typicode.com/\(endpoint)")!
    }
}

enum Endpoint {
       case posts
       case users
       case comments
   }

extension Endpoint {
   var url: URL {
       switch self {
       case .posts:
           return .makeForEndpoint("posts")
       case .users:
           return .makeForEndpoint("users")
       case .comments:
           return .makeForEndpoint("comments")
       }
   }
}

extension URLSession {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void

    @discardableResult
    func request(
        _ endpoint: Endpoint,
        then handler: @escaping Handler
    ) -> URLSessionDataTask {
        let task = dataTask(
            with: endpoint.url,
            completionHandler: handler
        )

        task.resume()
        return task
    }
}
